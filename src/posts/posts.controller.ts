import { Controller, Get, Post } from '@nestjs/common';
import { Post as PostModel } from './post.model';
import { PostsService } from './posts.service';
import { RedditRestService } from './reddit/reddit-rest/reddit-rest.service';


@Controller('posts')
export class PostsController {
    constructor(private readonly postsService: PostsService, 
        private readonly redditRestService:RedditRestService) { }

    @Get()
    async findAll(): Promise<PostModel[]> {
        return this.postsService.findAll();
    }

    @Post("save")
    async save(){
        return this.postsService.saveAll();
    }
}
