import { HttpModule, Module } from '@nestjs/common';
import { ScheduleModule } from 'nest-schedule';
import { TypegooseModule } from 'nestjs-typegoose';
import { Post } from './post.model';
import { PostsController } from './posts.controller';
import { PostsService } from './posts.service';
import { RedditRestService } from './reddit/reddit-rest/reddit-rest.service';
import { UrlBuilder } from './reddit/url.builder';
import { ScheduleService } from './scheduler/schedule/schedule.service';
import { TwitterRestService } from 'src/posts/twitter/twitter-rest/twitter-rest.service';
import { TwitterUrlBuilder } from './twitter/url.twitter.builder';


@Module({
    imports: [HttpModule, ScheduleModule.register(), TypegooseModule.forFeature([Post])],
    controllers: [PostsController],
    providers: [
        PostsService,
        RedditRestService,
        TwitterRestService,
        ScheduleService,
        UrlBuilder,
        TwitterUrlBuilder
    ],
})
export class PostsModule {}
