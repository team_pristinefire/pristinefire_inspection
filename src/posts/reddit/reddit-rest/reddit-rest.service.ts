import { HttpService, Injectable } from '@nestjs/common';
import { Carrier } from 'src/posts/interface/carrier.interface';
import { Post } from 'src/posts/post.model';
import { UrlBuilder } from '../url.builder';

@Injectable()
export class RedditRestService implements Carrier {

    constructor(private http: HttpService, private urlBuilder: UrlBuilder) { }

    async getPosts(): Promise<Post[]> {
        var that = this;
        let after: string = null;
        let allPost = [];
    
        do{
            let postUrl = (await this.urlBuilder.getDiamondPostQuery(null, "day", "100", after, null)).toString();
            let data = await this.http.get(postUrl).toPromise();
            after = data.data.data.after;
            allPost = allPost.concat(data.data.data.children);
        }while(after != null);

        let result: any = Array<Post>();
        await Promise.all(allPost.map(async (element) => {
            try {
                let postCommentUrl = (await this.urlBuilder.getDiamondCommentQuery(element.data.id)).toString();
                let postWithComment = await that.http.get(postCommentUrl).toPromise();
                let postResponse = await that.mapper(postWithComment.data, new Array<Post>(), null);
                result = result.concat(postResponse);
            } catch (error) {
                console.log('error==' + error);
            }
        }))
        return await Promise.all(result);
    }



    mapper(postAndComments: Array<{ data: any }>, postStore: Array<Post>, tempId: string) {
       
        postAndComments.forEach(post => {
            let postData = post.data;
            'children' in postData ? console.log('Child detected') : console.log('Child missing');
            'replies' in postData ? console.log('replies detected') : console.log('replies missing');
            if ('children' in postData) {
                let postChildren: Array<any> = post.data.children;
                if (post.data.dist != null) {
                    let postObject = new Post();
                    postObject._id = postChildren[0].data.id;
                    postObject.title = postChildren[0].data.title;
                    postObject.content = postChildren[0].data.selftext;
                    postObject.timestamp = postChildren[0].data.created_utc;
                    postStore.push(postObject);
                    if ( typeof tempId !='undefined' && tempId) {
                        postObject.parentId = tempId;
                        tempId = postChildren[0].data.id;
                        console.log("tempId=="+tempId);
                    }else{
                        tempId = postChildren[0].data.id;
                    }
                } else {
                    this.mapper(postChildren, postStore, tempId);
                }
            } else {
                let postReplies = post.data.replies;
                let postObject = new Post();
                postObject._id = post.data.id;
                postObject.title = post.data.body;
                postObject.content = post.data.body;
                postObject.timestamp = post.data.created_utc;
                if (typeof tempId !='undefined' && tempId) {
                    postObject.parentId = tempId;
                    tempId = post.data.id;
                }
                postStore.push(postObject);
                if (Object.keys(postReplies).length != 0) {
                    this.mapper(new Array<{ data: any }>(postReplies), postStore, tempId);
                }
            }
        });
        return postStore;
    }

}
