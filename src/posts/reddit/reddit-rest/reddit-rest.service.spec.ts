import { Test, TestingModule } from '@nestjs/testing';
import { RedditRestService } from './reddit-rest.service';

describe('RedditRestService', () => {
  let service: RedditRestService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [RedditRestService],
    }).compile();

    service = module.get<RedditRestService>(RedditRestService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
