import { URL } from "src/constants";
import { Injectable } from "@nestjs/common";

@Injectable()
export class UrlBuilder {
   
    constructor() {}        
    
    async getDiamondPostQuery(q: string, t: string, limit: string, after:string, before:string): Promise<string> {
        let diamondPostQuery = URL.REDDIT.DIAMOND.POST ;
        var prepend = '';
        if (q)  { 
            diamondPostQuery = `${diamondPostQuery}` + prepend + `q=${q}`;
            prepend = '&';
        }
        if (t)  {
            diamondPostQuery = `${diamondPostQuery}` + prepend + `t=${t}`;
            prepend = '&';
        }
        if (limit)  {
            diamondPostQuery =  `${diamondPostQuery}` + prepend + `limit=${limit}`;
            prepend = '&';
        }
        if (after)  {
            diamondPostQuery =  `${diamondPostQuery}` + prepend + `after=${after}`;
            prepend = '&';
        }
        if (before)  {
            diamondPostQuery =  `${diamondPostQuery}` + prepend + `before=${before}`;
            prepend = '&';
        }
        console.log(diamondPostQuery);
        return diamondPostQuery;
    }

    async getDiamondCommentQuery(id: string): Promise<string> {
        let diamondCommentQuery = URL.REDDIT.DIAMOND.COMMENT;
        return diamondCommentQuery.replace('$id$', id);
    }
}