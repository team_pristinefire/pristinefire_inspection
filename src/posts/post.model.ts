import { prop, getModelForClass, Typegoose } from '@typegoose/typegoose';

export class Post extends Typegoose{
    @prop()
    _id?: string;
    @prop()
    title?: string;
    @prop()
    content?: string;
    @prop()
    timestamp?: string;
    @prop()
    parentId?:String;    
}

