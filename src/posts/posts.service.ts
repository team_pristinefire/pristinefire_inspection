import { Injectable } from '@nestjs/common';
import { ReturnModelType, mongoose } from '@typegoose/typegoose';
import { InjectModel } from 'nestjs-typegoose';
import { Post as PostModel } from './post.model';
import { RedditRestService } from './reddit/reddit-rest/reddit-rest.service';
import { TwitterRestService } from 'src/posts/twitter/twitter-rest/twitter-rest.service';


@Injectable()
export class PostsService {
    constructor(
        private readonly redditRestService: RedditRestService, private readonly twitterRestService: TwitterRestService,
        @InjectModel(PostModel) private readonly postModel: ReturnModelType<typeof PostModel>) { }


    async findAll(): Promise<PostModel[]> {
        return await this.postModel.find().exec();
    }

    async saveAll() {
        var that = this;
        let posts: Array<PostModel> = await this.redditRestService.getPosts();
        console.log(((posts)).length)

        posts.forEach(async function (post: PostModel) {
            const createdPost = new that.postModel(post);
            that.postModel.findOneAndUpdate({
                _id: post._id.toString()
            }, createdPost, { upsert: true }, function (err, res) {
                if (err) {
                    console.log("MongoError:" + err);
                }
            });
        });

        let tweets: Array<PostModel> = await this.twitterRestService.getPosts();
        console.log(((tweets)).length)

        tweets.forEach(async function (tweet: PostModel) {
            const createdPost = new that.postModel(tweet);
            that.postModel.findOneAndUpdate({
                _id: tweet._id.toString()
            }, createdPost, { upsert: true }, function (err, res) {
                if (err) {
                    console.log("MongoError:" + err);
                }
            });
        });
    }

}
