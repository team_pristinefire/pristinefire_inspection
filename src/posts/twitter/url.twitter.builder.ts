import { URL } from "src/constants";
import { Injectable } from "@nestjs/common";

@Injectable()
export class TwitterUrlBuilder {
   
    constructor() {}        
    
    async getTweetsQuery(q: string, until: string, count: string): Promise<string> {
        let tweetQuery = URL.TWITTER.SEARCH ;
        var prepend = '';
        if (q)  { 
            tweetQuery = `${tweetQuery}` + prepend + `q=${q}`;
            prepend = '&';
        }
        if (until)  {
            tweetQuery = `${tweetQuery}` + prepend + `until=${until}`;
            prepend = '&';
        }
        if (count)  {
            tweetQuery =  `${tweetQuery}` + prepend + `count=${count}`;
            prepend = '&';
        }
       
        console.log(tweetQuery);
        return tweetQuery;
    }

}