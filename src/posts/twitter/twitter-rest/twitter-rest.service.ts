import { Injectable, HttpService } from '@nestjs/common';
import { Carrier } from 'src/posts/interface/carrier.interface';
import { URL } from "src/constants";
import { TwitterUrlBuilder } from '../url.twitter.builder';
import { Post } from 'src/posts/post.model';


@Injectable()
export class TwitterRestService implements Carrier {

    constructor(private http: HttpService, private urlBuilder: TwitterUrlBuilder) { }

    async getPosts() : Promise<Post[]>{
        let accessToken: string = await this.getAccessToken();
        let result: any = Array<Post>();
        let date: string = new Date().toISOString().replace('T', ' ').substr(0, 10);
        const headersRequest = {
            'Content-Type': 'application/json',
            'Authorization': accessToken,
        };
        let tweetUrl = await this.urlBuilder.getTweetsQuery("buying%20diamond", date, "2");
        let tweets = this.http.get(tweetUrl, { headers: headersRequest }).toPromise();
        let tweetResponse = this.mapper((await tweets).data.statuses);
        return tweetResponse;
    }


    mapper(tweets: Array<any>) {
        let tweetList = [];
        tweets.forEach(tweet =>{
            let tweetObject = new Post();
            tweetObject._id = tweet.id;
            tweetObject.content = tweet.text;
            tweetObject.timestamp = tweet.created_at;
            tweetList = tweetList.concat(tweetObject)
        });
        return tweetList;
    }

    private async getAccessToken(): Promise<any> {
        let basicToken = `Basic` + ' ' + Buffer.from(URL.TWITTER.API_KEY + ':' + URL.TWITTER.API_SECRET).toString("base64");
        const headersRequest = {
            'Content-Type': 'application/json',
            'Authorization': basicToken,
            'Content-Length': '29',
            'User-Agent': 'Pristinefire_tweets',
            'charset': 'UTF-8',
            'Accept-Encoding': 'gzip'
        };
        const result = await this.http.post(URL.TWITTER.OAUTH2, " ", { headers: headersRequest }).toPromise();
        return 'Bearer' + ' ' + result.data.access_token;
    }
}
