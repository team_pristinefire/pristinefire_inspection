import { Test, TestingModule } from '@nestjs/testing';
import { TwitterRestService } from './twitter-rest.service';

describe('TwitterRestService', () => {
  let service: TwitterRestService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [TwitterRestService],
    }).compile();

    service = module.get<TwitterRestService>(TwitterRestService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
