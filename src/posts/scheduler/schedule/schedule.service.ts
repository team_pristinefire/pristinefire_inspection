import { Injectable } from '@nestjs/common';
import { Cron, NestSchedule, UseLocker } from 'nest-schedule';
import {PostsService} from 'src/posts/posts.service'

@Injectable()
export class ScheduleService extends NestSchedule {

    constructor(private readonly postService:PostsService){
        super();
    }

  @Cron('27 05 * * *')
  async getPostsForDay() {
    console.log('start executing cron job');
    await this.postService.saveAll();
    console.log('end executing cron job');
  }

}
