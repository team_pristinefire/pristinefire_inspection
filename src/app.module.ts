import { Module } from '@nestjs/common';
import { TypegooseModule } from 'nestjs-typegoose';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { PostsModule } from './posts/posts.module';
import { DB_PROVIDER } from './constants';
import { TwitterRestService } from './posts/twitter/twitter-rest/twitter-rest.service';

@Module({
  imports: [PostsModule, 
           TypegooseModule.forRoot(DB_PROVIDER)],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
